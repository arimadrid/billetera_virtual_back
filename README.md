# ePayco

## Descripción

> Repositorio back-end para proyecto de simulación de billetera virtual que permite realizar las operaciones de:
### Registro de clientes:
Ingresando su documento, nombres teléfono y email.
### Recargar saldo a su billetera.
Mediante el uso de su documento, teléfono y el monto que desea recargar.
### Consultar saldo.
Colocando su documento y teléfono el sistema le indica el saldo actual.
### Pagar compras.
Realice sus pagos ingresando su documento, télefono y el monto a pagar. El sistema le enviará un codigo de confirmación a su correo electrónico, el cual deberá ingresar en la ventana emergente que muestra a página para poder confirmar su pago.

## Para usar el sistema siga los siguientes pasos:

> Clone los repositorios, tanto front-end como back-end.
> Para usar el back-end instale las dependencias de la siguiente manera:

## Build Setup

```bash

# install dependencies
$ composer install

# migrations

1. Cree su base de datos y configure los datos en el archivo .env tal y como se indican en el .env.example.

$ php artisan key:generate

$ php artisan migrate

# serve with at localhost:8002
$ php artisan serve --port=8002

```