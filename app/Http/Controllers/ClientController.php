<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    
    public function store(Request $request)
    {
        $request->validate([
      		'name' => 'required',
      		'tlf' => 'required',
      		'email' => 'required',
      		'document' => 'required'
    	]);
    
        $client = new Client();
        $client->name = $request->name;
        $client->tlf = $request->tlf;
        $client->email =$request->email;
        $client->document =$request->document;
        $client->save();

        // $this->logger($request->user(), 'clientStore', $client);

        return response()->json([
            'result' => 'OK',
            'message' => 'Cliente registrado.'
        ]);
    }

    public function list()
    {
    	$client = Client::all();
    	return $client;
    }


    public function update(Request $request)
    {
        $document = $request->document;
        $tlf = $request->tlf;
       
        $client = Client::where('document', $document)->where('tlf', $tlf)->get();
        if($client != []){
            foreach ($client as $client) {
                    $client->balance = $client->balance + $request->balance;
                    $client->save();
                    return response()->json([
                        'result' => 'OK',
                        'message' => 'Saldo agregado.'
                    ]);
            }
        }else{
            return response()->json([
            'result' => 'error',
            'message' => 'No existen los credenciales'
                ]);
        }
        

    }

}
