<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Wallet;

class WalletController extends Controller
{
   public function store(Request $request)
   {
   	// return $request->document;
   	$client = Client::first();
   	 // return $client->document;
   		if($client->document == $request->document)
   		{
   			$wallet = new Wallet();
   			$wallet->balance = $request->balance;
   			$wallet->client_id = $request->client_id;
        	$wallet->save();

        	return response()->json([
            'result' => 'OK',
            'message' => 'Recarga de Billetera satisfactoria.'
        ]);
   		}
   		else{
   			return false;
   		}

   }
}
