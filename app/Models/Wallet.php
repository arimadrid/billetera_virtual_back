<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class wallet extends Model
{
    use HasFactory;

    protected $fillable = [
    	'balance',
        'client_id'
	];

 public function clients()
    {
        return $this->belongsToMany('App\Client');
    }

}
